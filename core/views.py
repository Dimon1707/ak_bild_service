from django.shortcuts import render
from .forms import ContactForm


def contacts(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'contacts.html')
    form = ContactForm()
    context = {'form': form}
    return render(request, 'contacts.html', context)


def main(request):
    return render(request, 'main.html')


# def contacts(request):
#     return render(request, 'contacts.html')


def cars(request):
    return render(request, 'cars.html')


def about(request):
    return render(request, 'about.html')
