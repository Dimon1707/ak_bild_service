from django.urls import path
from core.views import main, contacts, cars, about

urlpatterns = [
    path('', main, name='main'),
    path('contacts/', contacts, name='contacts'),
    path('cars/', cars, name='cars'),
    path('about/', about, name='about')
]
